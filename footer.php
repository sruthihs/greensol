<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package awsm
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
					<nav>
						<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_id' => 'footer-menu' ) ); ?>
					</nav>
					<ul class="list-inline social-links">
						<?php printmeta('facebook_url', '<li><a href="%s" title="Facebook"><i class="icon-facebook"></i></a></li>', false, 'option');?>
						<?php printmeta('linkedin_url', '<li><a href="%s"><i class="icon-linkedin" title="Linkedin"></i></a></li>', false, 'option');?>
						<?php printmeta('youtube_url', '<li><a href="%s"><i class="icon-youtube" title="YouTube"></i></a></li>', false, 'option');?>
					</ul>
				</div><!-- .col -->
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-5">
					<?php 
						$contact = get_field('contact_number', 'option');
						$email = get_field('contact_email', 'option');
						if($contact || $email){
					?>
						<div class="footer-contact-info">
							<h2>For sales enquiries</h2>
							<p><?php 
								printmeta('contact_number', 'Call:%s or ', false, 'option');
								printmeta('contact_email', 'Email: <a href="mailto:'.$email.'">%s</a>', false, 'option');
							?></p>
						</div>
					<?php }?>
					<div class="hidden-xs">
						<h2>Greensol Updates</h2>
						<?php 
						$args = array(
						    'post_type'  => 'post',
						    'posts_per_page' => 1,
						);
							
						$blogs = new WP_Query( $args );
						if ( $blogs->have_posts() ) :?>
								<?php  while ( $blogs->have_posts() ) : $blogs->the_post();?>
									<div class="blog-item">
										<a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>">
											<?php the_post_thumbnail('thumbnail');?>
										</a>
											<div class="blog-item-content">
												<?php 
													the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
													echo '<p>'.wp_trim_words( get_the_content(), '19', '').'</p>';
												?>
												<a href="<?php echo get_permalink(20);?>" class="button button-green">More Updates</a>
											</div><!-- .blog-grid-content -->
									</div><!-- .blog-item -->
								<?php endwhile;?>
								
						<?php endif; wp_reset_postdata();?>
					</div>
				</div><!-- .col -->
				<div class="col-xs-12 col-sm-6 col-md-5" id="book-a-visit">
					<h2>Request for an enquiry/site visit/quotation</h2>
					<?php echo do_shortcode('[contact-form-7 id="4" title="Request a visit"]');?>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</footer><!-- #colophon -->
	<a href="#book-a-visit" class="scroll-to book-a-visit-btn button button-green">Book a visit</a>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
