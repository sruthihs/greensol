<?php
/**
 * Template Name: Services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
		<main id="main" class="site-main banner-fix" role="main">

			<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-head">
					<div class="container-fluid">
						<?php 
							the_title('<h1>','</h1>');
							printmeta('banner_description', '<p>%s</p>');
						?>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main">
					<div class="container-fluid">
						<?php 
							$services = get_terms('service_type', array('hide_empty' => false,) );
							if($services):
							?>
							<div class="row">
								<?php foreach ($services as $service):?>
									<div class="col-xs-12 col-sm-6 col-md-4">
										<div class="service-item">
											<h2><?php echo $service->name;?></h2>
											<?php 
												$args = array(
												    'post_type'  => 'service',
												    'posts_per_page' => -1,
												    'tax_query' => array(
														array(
															'taxonomy' => 'service_type',
												            'field' => 'id',
												            'terms' => $service->term_id,
														),
													),
												);
												$brocure = get_field('brochure', 'service_type_'.$service->term_id);
												$bg = get_field('grid_background', 'service_type_'.$service->term_id);
												$service = new WP_Query( $args );
												$class= " ";
												if ( !$service->have_posts()){
													$class= " btn-only";
												}
												if ( $service->have_posts() || $brocure){?><ul class="service-list list-unstyled<?php echo $class;?>"><?php }?>
												<?php if ( $service->have_posts() ) :?>
												
													<?php $count=0; while ( $service->have_posts() ) : $service->the_post(); $count++;
														
														the_title( sprintf( '<li><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></li>' );

													?>

											
													<?php endwhile; 
														?>
												
												
											<?php endif; wp_reset_postdata(); 
													if($brocure){
															echo '<li class="brochure-download"><a href="'.$brocure['url'].'" target="_blank"><i class="icon-download"></i> Download Brochure</a></li>';
														}?>
											<?php if ( $service->have_posts() || $brocure){?></ul><?php }?>
											<div class="service-bg" style="background-image: url(<?php echo $bg['url'];?>);"></div>

										</div><!-- .service-item -->
									</div><!-- .coll -->
								<?php endforeach;?>
							</div><!-- row -->
						<?php endif;?>
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
