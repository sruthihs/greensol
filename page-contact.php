<?php
/**
 * Template Name: Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
		<main id="main" class="site-main banner-fix" role="main">

			<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-head">
					<div class="container-fluid">
						<?php 
							the_title('<h1>','</h1>');
							printmeta('banner_description', '<p>%s</p>');
						?>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main contact-main">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-md-5">
								<div class="contact-info">
									<?php the_content();?>
								</div>
								<div class="contact-info">
									<h2>Get in touch</h2>
									<?php printmeta('email', '<p><i class="icon-phone"></i>%s</p>'); ?>
									<ul class="list-inline">
										<?php printmeta('phone_india', '<li><span class="in-flg"><img src="'.get_template_directory_uri().'/images/in.gif" alt="in"></span>%s</li>'); ?>
										<?php printmeta('phone_uae', '<li><span class="uae-flg"><img src="'.get_template_directory_uri().'/images/uae.gif" alt="uae"></span>%s</li>'); ?>
									</ul>
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<h2>Send us a message</h2>
								<?php echo do_shortcode('[contact-form-7 id="109" title="Contact"]');?>
							</div>
						</div>
							
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
				<?php 
					$lat = get_field('map_latitude');
					$lng = get_field('map_longitude');
					if($lat && $lng){
				?>
				<div class="g-map-main" id="g-map"></div>
				<script type="text/javascript">
					function initialize() {
        var myLatLng = {lat: <?php echo $lat;?>, lng: <?php echo $lng;?>};

        var map = new google.maps.Map(document.getElementById('g-map'), {
          zoom: 18,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Greensol Renewable Power Pvt Ltd'
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
				</script>
				<?php }?>
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
