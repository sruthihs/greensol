<?php
/**
 * Template Name: Gallery
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
<main id="main" class="site-main banner-fix" role="main">
	<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-head">
					<div class="container-fluid">
						<?php 
							the_title('<h1>','</h1>');
							printmeta('banner_description', '<p>%s</p>');
						?>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main">
					<div class="container-fluid">
						<?php 
								$args = array(
								    'post_type'  => 'galleries',
								    'posts_per_page' => 10,
								);
									
								$galleries = new WP_Query( $args );
								if ( $galleries->have_posts() ) :?>
								<div id="loadmorecontainer" class="flex-row gallery-grids">
									<?php while ( $galleries->have_posts() ) : $galleries->the_post();?>
										<div class="col-xs-12 col-sm-6 flex-item">
											<a href="<?php the_permalink();?> #single-gallery" class="gallery-grid-item flex-inner" title="<?php the_title_attribute();?>">
												<?php the_post_thumbnail('gallery-main-thumb');?>
												<?php the_title('<h3>','</h3>');
													  printmeta('location','<p>%s</p>');
												?>
											</a>	
										</div><!-- .coll -->
									<?php endwhile;?>
									<?php
			                    $link=get_next_posts_link('link',$galleries->max_num_pages);
			                        if($link){
			                        echo '<div class="col-xs-12 flex-item load-more animate-it"><a href="'.get_next_posts_page_link().'" class="loadmore button button-green"><span>Load More</span></a></div>';
			                        }
			                ?>
								</div><!-- row -->
							<?php endif; wp_reset_postdata();?>
							<div class="gallery-main">
								<span class="gallery-close icon-close"></span>
								<div id="gallery-wrap" class="gallery-wrap">
									
								</div><!-- .gallery-wrap -->
							</div><!-- .gallery-main -->
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
