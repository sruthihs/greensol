<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
<main id="main" class="site-main banner-fix" role="main">
	<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-head">
					<div class="container-fluid">
						<?php 
							the_title('<h1>','</h1>');
							printmeta('banner_description', '<p>%s</p>');
						?>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main">
					<div class="container-fluid">
						<div class="single-page-content">
							<div class="entry-content">
								<?php the_content();?>
							</div>
						</div><!-- .single-page-content -->
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
