 (function($) {
    $('.home-slider-inner').slick({
      dots: false,
      infinite: true,
      speed: 500,
      arrows: true,
      cssEase: 'linear',
      autoplay: true,
      autoplaySpeed: 8000,
      prevArrow: '<span class="slick-prev slick-arrow"><i class="icon-angle-left"></i></span>',
      nextArrow: '<span class="slick-next slick-arrow"><i class="icon-angle-right"></i></span>',
    });

  $('.service-carousel').slick({   
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      initialSlide: 1,
      prevArrow: '<span class="slick-prev slick-arrow"><i class="icon-angle-left"></i></span>',
      nextArrow: '<span class="slick-next slick-arrow"><i class="icon-angle-right"></i></span>',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
          }
        },
       ]
    });


   $(document).on('click', '.loadmore', function (e) {
          e.preventDefault();
          var $el = $(this),
              link = $el.attr('href');
          if ($el.attr('disabled'))
              return;
          $el.addClass('loading').attr('disabled', true);
          $el.find('span').html('Loading...')
          $("<div>").load(link + " #loadmorecontainer", function () {
              $("#loadmorecontainer").append($(this).find("#loadmorecontainer").html());
              $el.parents('.load-more').removeAttr('disabled').remove();
          });
      });


   /**
        Share popup window
      **/
       var popupCenter = function(url, title, w, h) {
          // Fixes dual-screen position                         Most browsers      Firefox
          var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
          var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

          var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
          var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

          var left = ((width / 2) - (w / 2)) + dualScreenLeft;
          var top = ((height / 3) - (h / 3)) + dualScreenTop;

          var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

          // Puts focus on the newWindow
          if (window.focus) {
            newWindow.focus();
          }
        };
      $(document).on( "click", '.share-popup', function(e) {
          var _this = $(this);
          popupCenter(_this.attr('href'), _this.find('.text').html(), 580, 470);
          e.preventDefault();
        });
      $("#single-gallery").unitegallery();
      $(document).on('click', '.gallery-grid-item', function(e){
        e.preventDefault();
        var $url = $(this).attr('href');
        $('.gallery-main').addClass('active loading'); 
        $('#gallery-wrap').load( $url,function () {
            $('.gallery-main').removeClass('loading'); 
            $("#single-gallery").unitegallery();           
        });
      });
      $(document).on('click', '.gallery-main', function(e){
        var el = $('.single-gallery');
        if(!el.is(e.target) && el.has(e.target).length === 0){
          $('.gallery-main').removeClass('loading active');
          $( "#single-gallery" ).remove();
        }
    
      });
      $(document).on('click', '.gallery-close', function(e){
          $('.gallery-main').removeClass('loading active');
          $( "#single-gallery" ).remove();
      });
      $('.video-modal').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
    $(document).on('click', '.nav-toggle', function(e){
      e.preventDefault();
      $(this).toggleClass('open');
      $('.main-navigation').slideToggle();
    });
    $(document).on('click','.scroll-to',function(e){
      e.preventDefault();
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top-30;
        $('html, body').stop().animate({ 
            scrollTop: offsetTop
        }, 300);
    });
    $(document).on('click', '.cr-content-toggle', function(e){
      e.preventDefault();
      $(this).fadeOut().parent().next().slideToggle();
    });
  })(jQuery);