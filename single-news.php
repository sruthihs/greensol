<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package awsm
 */

get_header(); ?>
	<main id="main" class="site-main" role="main">

				<?php 
					while ( have_posts() ) : the_post(); 
					$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
				?>
					<div class="page-banner" data-parallax="scroll" data-image-src="<?php echo $banner[0];?>">
						<div class="page-banner-main">
							<div class="container-fluid">
								<div class="page-banner-inner">
									<?php 
										the_title( '<h1>', '</h1>' ); 
										printmeta('banner_description', '<p>%s</p>');
									?>
								</div>
							</div><!-- .container-fluid -->
						</div><!-- .page-banner-main -->
					</div>
					<div class="single-content">
						<div class="container-fluid">
							<div class="blog-single-main">
									<div class="entry-content blog-single-content">
										<?php
											echo '<span class="post-date-meta"><i class="icon-clock"></i>'.get_the_date('F d Y').'</span>';
											 the_content();?>
									</div>
									<div class="social-share">
										<span>SHARE</span>
										<ul class="list-inline">
											<li>
												<a class="share-popup fb" href="https://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo get_the_title($post->ID);?>&p[summary]=<?php echo get_the_excerpt($post->ID);?>&p[url]=<?php echo get_permalink($post->ID);?>"><i class="icon-facebook"></i></a>
											</li>
											<li>
												<a class="share-popup tw" href="http://twitter.com/share?text=<?php echo get_the_title($post->ID);?>&url=<?php echo get_permalink($post->ID);?>"><i class="icon-twitter"></i></a>
											</li>
										</ul>
									</div><!-- .social-share -->
							</div><!-- .blog-single-main -->
							
						</div><!-- .container-fluid -->
					</div><!-- .single-content -->
					<?php /*<div class="single-bottom">
						<div class="container-fluid">
							<h2 class="text-center">You might be interested in these too</h2>
						
								<?php 

								 global $post;
								    $orig_post = $post;
								    $cats      = wp_get_post_terms($post->ID, 'category');
								    
								    $related     = $cats;
								    if ($related):
								        $tag_ids = array();
								        foreach ($related as $individual_tag) {
								            $tag_ids[] = $individual_tag->term_id;
								        }

								        $args = array(
								            'post_type'      => 'news',
								            'post__not_in'   => array($post->ID),
								            'orderby'        => 'rand',
								            'posts_per_page' => 3,
								            'tax_query' => array(
								                    array(
								                        'taxonomy' => 'category',
								                        'field' => 'id',
								                        'terms' => $tag_ids,
								                        'operator'=> 'IN' //Or 'AND' or 'NOT IN'
								                     )),
								            );
								        $related_items = new wp_query($args);
								        if ($related_items):?>
								<div  class="flex-row blog-grids">
									<?php while ($related_items->have_posts()): $related_items->the_post();?>
										<div class="col-xs-12 col-sm-6 col-md-4 flex-item">
											<a href="<?php the_permalink();?>" class="blog-grid-item flex-inner" title="<?php the_title_attribute();?>">
												<?php the_post_thumbnail('blog-thumb');?>
												<div class="blog-card-content">
													<?php 
														the_title('<h3>','</h3>');
														echo '<p>'.wp_trim_words( get_the_content(), '20', '' ).'</p>';
														echo '<span><i class="icon-clock"></i>'.get_the_date('F d Y').'</span>';
													?>
												</div><!-- .blog-card-content -->
											</a>	
										</div><!-- .coll -->
									<?php endwhile;?>
									
								</div><!-- row -->
							<?php endif; $post = $orig_post; wp_reset_postdata();?>
						<?php endif;?>
						</div>
					</div>*/?>
				<?php endwhile; // End of the loop. ?>

				</main><!-- #main -->
	
<?php get_footer(); ?>
