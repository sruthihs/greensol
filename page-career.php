<?php
/**
 * Template Name: Career
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
<main id="main" class="site-main" role="main">
	<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-head">
					<div class="container-fluid">
						<div class="page-head-inner">
							<?php 
								the_title('<h1>','</h1>');
								printmeta('banner_description', '<h2>%s</h2>');
							?>
						</div>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main">
					<div class="container-fluid">
						<div class="single-page-content">
							<div class="entry-content">
								<?php the_content();?>
							</div>
						</div><!-- .single-page-content -->
						<div class="career-image">
							<?php the_post_thumbnail();?>
						</div>
						<div class="single-page-content">
							<?php 
								$args = array(
								    'post_type'  => 'career',
								    'posts_per_page' => -1,
								);
									
								$career = new WP_Query( $args );
								if ( $career->have_posts() ) :?>
								<div class="current-openings">
									<h2>Current Openings</h2>
									<?php while ( $career->have_posts() ) : $career->the_post();?>
										<div class="opening-item">
											<div class="opening-item-head">
												<?php 
													the_title('<h3>', '</h3>');
													printmeta('short_description', '<p>%s</p>');
													printmeta('apply_link', '<a href="%s" class="button button-green">Apply Now</a>');
												?>
												<a href="#" class="cr-content-toggle">More Details</a>
											</div>
											<div class="opening-content">
												<div class="entry-content">
													<?php the_content();?>
												</div>
											</div>
										</div>
									<?php endwhile;?>
								</div>
							<?php endif; wp_reset_postdata();?>
						</div>
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
