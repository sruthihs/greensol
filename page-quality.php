<?php
/**
 * Template Name: Quality
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
<main id="main" class="site-main banner-fix" role="main">
	<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-head">
					<div class="container-fluid">
						<?php 
							the_title('<h1>','</h1>');
							printmeta('banner_description', '<p>%s</p>');
						?>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main">
					<div class="container-fluid">
						<div class="single-page-content">
							<div class="entry-content">
								<?php the_content();?>
								<?php if( have_rows('logos') ):?>
									<p>Our products are certified to:</p>
									<ul class="certificate-logos list-unstyled flex-row">
										<?php 
											while ( have_rows('logos') ) : the_row();
											$image = get_sub_field('image');
										?>
											<li class="flex-item"><span class="flex-inner certificate-logo"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>"></span></li>
										<?php endwhile;?>
									</ul>
								<?php endif;?>
							</div>
						</div><!-- .single-page-content -->
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
