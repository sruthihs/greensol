<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package awsm
 */

get_header(); ?>
	<main id="main" class="site-main banner-fix" role="main">

				<?php 
					while ( have_posts() ) : the_post(); 
					$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
				?>
					<div class="page-head">
					<div class="container-fluid">
						<?php 
							the_title('<h1>','</h1>');
							printmeta('location', '<p>%s</p>');
						?>
					</div><!-- .container-fluid -->
				</div><!-- .page-head -->
				<div class="service-main">
					<div class="container-fluid">
						<div class="single-page-content">
							<div class="entry-content">
								<?php the_content();?>
								<?php if( have_rows('gallery_images') ):?>
									<div id="single-gallery" class="single-gallery" style="display: none;">
										<?php while ( have_rows('gallery_images') ) : the_row();
											$gallery_type = get_sub_field('gallery_type');
											$image = get_sub_field('image');
											$video_id = get_sub_field('video_id');
											$video_preview = get_sub_field('preview_image');
											$description = get_sub_field('description');
											$attr = '';
											if($gallery_type == 'Image'){
												$attr .= ' data-image="'.$image['url'].'"';
												$attr .= ' alt="'.$image['title'].'"';
												$attr .= ' src="'.$image['sizes']['gallery-thumb'].'"';
											}else{
												$attr .= ' data-image="'.$image['url'].'"';
												$attr .= ' alt="Youtube Video"';
												$attr .= ' data-videoid="'.$video_id.'"';
												$attr .= ' data-type="youtube"';
												if($video_preview){
													$attr .= ' src="'.$video_preview['sizes']['gallery-thumb'].'"';
												}else{
													$attr .= ' src="https://img.youtube.com/vi/'.$video_id.'/0.jpg"';
												}
											}

										?>
											<img<?php echo $attr;?> data-description="<?php echo $description;?>">
											
										<?php endwhile;?>
									</div>
								<?php endif;?>
							</div>
						</div><!-- .single-page-content -->
					</div><!-- .container-fluid -->
				</div><!-- .service-main -->
				<?php endwhile; // End of the loop. ?>

				</main><!-- #main -->
	
<?php get_footer(); ?>
