<?php
/**
 * Template Name: Company
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
		<main id="main" class="site-main" role="main">

			<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<div class="page-banner company-banner" data-parallax="scroll" data-image-src="<?php echo $banner[0];?>">
						<div class="page-banner-main">
							<div class="container-fluid">
								<div class="page-banner-inner">
									<?php 
										//the_title( '<h1>', '</h1>' ); 
										printmeta('banner_description', '<p>%s</p>');
									?>
								</div>
							</div><!-- .container-fluid -->
						</div><!-- .page-banner-main -->
					</div>
				<div class="company-main">
					<div class="container-fluid">
						<div class="company-main-desc">
							<div class="company-main-desc-inner">
								<div class="entry-content">
									<?php the_content();?>
								</div>
							</div><!-- .company-main-desc-inner -->
						</div><!-- .company-main-desc -->
						<?php 
							$wwd_image = get_field('wwd_background');
							$wwd_title = get_field('wwd_title');
							$wwd_desc = get_field('wwd_description');
						?>
						<div class="company-wwd clearfix" style="background-image: url(<?php echo $wwd_image['url'];?>);">
							<?php if($wwd_title || $wwd_desc){?>
								<div class="company-wwd-desc">
									<?php 
										printmeta('wwd_title', '<h2>%s</h2>');
										printmeta('wwd_description', '<p>%s</p>');
									?>
								</div><!-- .company-wwd-desc -->
							<?php }?>

						</div><!-- .company-wwd -->
					</div><!-- .container-fluid -->
					<div class="company-our-mission">
						<div class="container-fluid">
							<div class="entry-content">
								<?php printmeta('om_title', '<h2>%s</h2>');?>
								<div class="row">
									<?php the_field('om_content');?>
								</div><!-- .row -->
							</div><!-- .entry-content -->
						</div><!-- .container-fluid -->
					</div><!-- .company-our-mission -->
					<?php $bg = get_field('ov_background');?>
					<div class="company-our-vision"  data-parallax="scroll" data-image-src="<?php echo $bg['url'];?>">
						<div class="container-fluid">
							<div class="company-our-vision-inner">
								<?php 
									printmeta('ov_title', '<h2>%s</h2>');
									printmeta('ov_description', '<p>%s</p>');
								?>
							</div>
						</div><!-- .container-fluid -->
					</div><!-- .company-our-mission -->
					<?php if( have_rows('logos') ):?>
						<div class="company-logos">
							<div class="container-fluid">
								<ul class="list-inline">
									<?php 
										while ( have_rows('logos') ) : the_row();
											$logo = get_sub_field('logo')
									?>
										<li><img src="<?php echo $logo['url'];?>" alt="<?php echo $logo['title'];?>"></li>
									<?php endwhile;?>
								</ul>
							</div><!-- .container-fluid -->
						</div><!-- .company-logos -->
					<?php endif;?>
				</div><!-- .company-main -->
			<?php endwhile;?>
		</main><!-- #main -->

<?php get_footer(); ?>
