<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package awsm
 */

get_header(); ?>
		<main id="main" class="site-main" role="main">

			<?php 
				while ( have_posts() ) : the_post(); 
				$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			?>

				<section class="home-banner" data-parallax="scroll" data-image-src="<?php echo $banner[0];?>">
					<div class="container-fluid">
						<div class="banner-conntent">
							<?php 
								printmeta('banner_title', '<h2>%s</h2>');
								printmeta('banner_description', '<p>%s</p>');
								printmeta('banner_url', '<a href="%s" class="button button-black">Learn More</a>');
							?>
						</div><!-- .banner-conntent -->
					</div><!-- .container-fluid -->
				</section>
				<section class="home-slider">
					<div class="home-slider-main">
					<?php if( have_rows('slideshow') ):?>
						<div class="home-slider-inner">
							<?php 
								while ( have_rows('slideshow') ) : the_row();
								$image = get_sub_field('image');
							?>
								<div class="slide-item">
									<div class="slide-image"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>"/></div>
									<div class="slider-content">
										<h2><?php the_sub_field('description');?></h2>
										<?php $brochure = get_sub_field('brochure');?>
										<ul class="list-inline">
											<li><a href="#" class="button button-green">View Services</a></li>
											<?php if($brochure){?>
												<li><a href="<?php echo $brochure['url'];?>" target="_blank" class="button button-black button-transparent">Download Brochure</a></li>
											<?php }?>
										</ul>
									</div>
								</div>
							<?php endwhile;?>
						</div><!-- .home-slider-inner -->
					<?php endif;?>
					</div><!-- .home-slider-main -->
				</section>
			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->

<?php get_footer(); ?>
