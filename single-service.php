<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package awsm
 */

get_header(); ?>

				<main id="main" class="site-main" role="main">

				<?php 
					while ( have_posts() ) : the_post(); 
					$banner = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
				?>
					<div class="page-banner service-banner" data-parallax="scroll" data-image-src="<?php echo $banner[0];?>">
						<div class="page-banner-main">
							<div class="container-fluid">
								<div class="page-banner-inner">
									<?php 
										the_title( '<h1>', '</h1>' ); 
										printmeta('banner_description', '<p>%s</p>');
										printmeta('project_video', '<p><a href="%s" class="button button-green video-modal">Watch project video</a></p>');
									?>
								</div>
							</div><!-- .container-fluid -->
						</div><!-- .page-banner-main -->
					</div>
					<div class="single-content">
						<div class="container-fluid">
							<div class="page-content-main">
								<div class="page-content-inner">
									<div class="entry-content">
										<?php the_content();?>

										<?php if( have_rows('salient_features') ):?>
											<h2>Salient Features</h2>
											<ul class="feature-list list-unstyled">
												<?php while ( have_rows('salient_features') ) : the_row();?>
													<li><?php the_sub_field('title');?></li>
												<?php endwhile;?>
											</ul>
										<?php endif;?>
										<div class="row">
											<div class="col-xs-12 col-md-4">
												<?php if( have_rows('features') ):?>
													<h2>Salient Features</h2>
													<ul class="feature-list list-unstyled">
														<?php while ( have_rows('features') ) : the_row();?>
															<li><?php the_sub_field('title');?></li>
														<?php endwhile;?>
													</ul>
												<?php endif;?>
											</div><!-- .col -->
											<div class="col-xs-12 col-md-6 col-md-offset-1">
												<?php if( have_rows('benefits') ):?>
													<h2>Benefits</h2>
													<ul class="feature-list list-unstyled">
														<?php while ( have_rows('benefits') ) : the_row();?>
															<li><?php the_sub_field('title');?></li>
														<?php endwhile;?>
													</ul>
												<?php endif;?>
											</div><!-- .col -->
										</div><!-- .row -->
									</div>
								</div><!-- .page-content-inner -->
							</div><!-- .page-content-main -->
							<div class="contact-info-bar">
								<div class="row">
									<div class="col-xs-12 col-md-6">
										<h2>Do you want help <br/>to find the right solution?</h2>
									</div><!-- .col -->
									<div class="col-xs-12 col-md-6">
										<div class="call-wrap">
											<span>Call us and we will help you</span>
											<p>+91 (080) 699 99750</p>
										</div>
									</div><!-- .col -->
								</div><!-- .row -->
							</div><!-- .contact-info-bar -->
						</div><!-- .container-fluid -->
					</div><!-- .single-content -->
					<div class="service-carousel-main single-bottom">
						<div class="container-fluid">
							<h2>More Services we offer</h2>
						</div>
								<?php 
							$services = get_terms('service_type', array('hide_empty' => true,) );
							if($services):
							?>
							<div class="service-carousel">
								<?php foreach ($services as $service):?>
									<div class="carousel-item">
										<div class="service-item">
											<h2><?php echo $service->name;?></h2>
											<?php 
												$args = array(
												    'post_type'  => 'service',
												    'posts_per_page' => -1,
												    'tax_query' => array(
														array(
															'taxonomy' => 'service_type',
												            'field' => 'id',
												            'terms' => $service->term_id,
														),
													),
												);
													
												$service = new WP_Query( $args );
												if ( $service->have_posts() ) :?>
												<ul class="service-list list-unstyled">
													<?php $count=0; while ( $service->have_posts() ) : $service->the_post(); $count++;
														if($count == 1){
															$bg = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
														}
														the_title( sprintf( '<li><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></li>' );
													?>

											
													<?php endwhile;?>
												</ul>
												<div class="service-bg" style="background-image: url(<?php echo $bg[0];?>);"></div>
											<?php endif; wp_reset_postdata();?>

										</div><!-- .service-item -->
									</div><!-- .coll -->
								<?php endforeach;?>
							</div><!-- service-carousel -->
						<?php endif;?>

					</div>
				<?php endwhile; // End of the loop. ?>

				</main><!-- #main -->
	
<?php get_footer(); ?>
